from langchain.document_loaders import AzureBlobStorageContainerLoader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores.azuresearch import AzureSearch

import logging, os
import azure.functions as func
from dotenv import load_dotenv

def main(req: func.HttpRequest, context: func.Context) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    func_path = context.function_directory
    logging.info(func_path)
    load_dotenv(f'{func_path}/.env')
    index_name = req.params.get('index_name') or "hr"
    if not index_name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            index_name = req_body.get('index_name') or "hr"

    connection_string="DefaultEndpointsProtocol=https;AccountName=globestorageapplication;AccountKey=nHr4mXmk6a6JF0nSVzw1RX8W71SYyUhJe/KheCt4Rzt8yfeLBOU68WqsFpzbmkt5o707H1s8hQ8f+AStqCaY4g==;EndpointSuffix=core.windows.net"
    
    container = "hr-documents" if index_name == "hr" else "ecom-documents"
    
    loader = AzureBlobStorageContainerLoader(
    conn_str=connection_string, container=container,
    )
    
    docs = loader.load()

    text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=0)
    splitDocs = text_splitter.split_documents(docs)
    
    embeddings = OpenAIEmbeddings(openai_api_key=os.getenv("OPENAI_API_KEY"), deployment="text-embedding-ada-002", chunk_size=1)
    
    azure_config = {
        "azure_search_endpoint": os.getenv("AZURE_SEARCH_ENDPOINT") if index_name == "hr" else os.getenv("AZURE_SEARCH_ECOM_ENDPOINT"),
        "azure_search_key": os.getenv("AZURE_SEARCH_KEY") if index_name == "hr" else os.getenv("AZURE_SEARCH_ECOM_KEY"),
        "index_name": os.getenv("AZURE_SEARCH_INDEX_NAME") if index_name == "hr" else os.getenv("AZURE_SEARCH_ECOM_INDEX_NAME"),
        "embedding_function": embeddings.embed_query
    }
    vector_store = AzureSearch(**azure_config)
    vector_store.add_documents(documents=splitDocs)

    return func.HttpResponse(
        f"This HTTP triggered function executed successfully. {index_name.upper()} data has been chunked and stored.",
        status_code=200
    )