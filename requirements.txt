# DO NOT include azure-functions-worker in this file
# The Python Worker is managed by Azure Functions platform
# Manually managing azure-functions-worker may cause unexpected issues

azure-functions
langchain
python-dotenv
azure-search-documents==11.4.0b6
azure-identity
openai
tiktoken
azure-storage-blob
